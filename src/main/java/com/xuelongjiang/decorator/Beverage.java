package com.xuelongjiang.decorator;

/**
 *
 *饮料类接口
 * @Author xuelongjiang
 */
public interface Beverage {

    public int cost();

}
