package com.xuelongjiang.decorator;

/**
 * 茶
 * @Author xuelongjiang
 */
public class Tea implements  Beverage {

    public int cost() {
        return 4;
    }
}
