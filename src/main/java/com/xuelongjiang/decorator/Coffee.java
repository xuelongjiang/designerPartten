package com.xuelongjiang.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * 咖啡
 * @Author xuelongjiang
 */
public class Coffee implements  Beverage {


    public int cost() {
        return 5;
    }
}
