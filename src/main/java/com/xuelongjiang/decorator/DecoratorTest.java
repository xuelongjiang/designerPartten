package com.xuelongjiang.decorator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 装饰者模式测试
 * @Author xuelongjiang
 */
public class DecoratorTest {

    private static Logger logger = LoggerFactory.getLogger(DecoratorTest.class);

    public static void main(String[] args) {

        logger.info("纯咖啡");
        Beverage coffee = new Coffee();
        logger.info(""+coffee.cost());

        logger.info("咖啡加牛奶");
        coffee = new Milk(coffee);
        logger.info(""+coffee.cost());

        logger.info("咖啡加牛奶加摩卡");
        coffee = new Mocha(coffee);
        logger.info(""+coffee.cost());


    }

}
