package com.xuelongjiang.decorator;

/**
 * 摩卡
 * @Author xuelongjiang
 */
public class Mocha extends   Condiment {

    private  Beverage beverage;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    public int cost() {
        return 3+beverage.cost();
    }
}
