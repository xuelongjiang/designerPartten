package com.xuelongjiang.decorator;

/**
 * 牛奶
 * @Author xuelongjiang
 */
public class Milk extends   Condiment {

    private Beverage beverage;

    public Milk(Beverage beverage) {
        this.beverage = beverage;
    }

    public int cost() {
        return 2+ beverage.cost();
    }
}
