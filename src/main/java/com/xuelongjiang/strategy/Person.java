package com.xuelongjiang.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author xuelongjiang
 */
public class Person {

    private  static Logger logger = LoggerFactory.getLogger(Person.class);

    private  Weapon weapon;

    public Person() {
        weapon = new Fist();
    }


    public void  changeWeapon(Weapon weapon){
        this.weapon = weapon;
    }

    public void attach(){
        weapon.attack();
    }

}
