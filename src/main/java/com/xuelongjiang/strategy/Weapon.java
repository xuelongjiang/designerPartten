package com.xuelongjiang.strategy;

/**
 * @Author xuelongjiang
 */
public interface Weapon {

    /**
     * 击打，有伤害
     */
    public void attack();


}
