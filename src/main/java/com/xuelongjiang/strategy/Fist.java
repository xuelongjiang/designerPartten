package com.xuelongjiang.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 拳头
 * @Author xuelongjiang
 */
public class Fist implements  Weapon {

    Logger logger = LoggerFactory.getLogger(Fist.class);

    /**
     * 击打，有伤害
     */
    public void attack() {
        logger.info("拳头的伤害为 10");
    }
}
