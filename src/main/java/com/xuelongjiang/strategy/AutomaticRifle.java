package com.xuelongjiang.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 *  自动步枪
 * @Author xuelongjiang
 */
public class AutomaticRifle implements  Weapon {

    private static Logger logger = LoggerFactory.getLogger(AutomaticRifle.class);

    /**
     * 击打，有伤害
     */
    public void attack() {
        logger.info("自动步枪的伤害为 100");

    }
}
