package com.xuelongjiang.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 手枪
 *
 * @Author xuelongjiang
 */
public class Pistol implements  Weapon {

    private static Logger logger = LoggerFactory.getLogger(Pistol.class);

    /**
     * 击打，有伤害
     */
    public void attack() {
        logger.info("手枪的伤害为50");
    }
}
