package com.xuelongjiang.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 模拟游戏中角色使用武器的
 *
 * 策略模式
 * @Author xuelongjiang
 */
public class Game {

    private static Logger logger = LoggerFactory.getLogger(Game.class);
    public static void main(String[] args) {

        logger.info("游戏开始，角色赤手空拳");
        Person person = new Person();
        person.attach();
        logger.info("角色获得木棍");
        person.changeWeapon(new Club());
        person.attach();
        logger.info("获得手枪");
        person.changeWeapon(new Pistol());
        person.attach();
        logger.info("获得自动步枪");
        person.changeWeapon(new AutomaticRifle());
        person.attach();

    }


}
