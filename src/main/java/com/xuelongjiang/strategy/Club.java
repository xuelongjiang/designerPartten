package com.xuelongjiang.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * 木棍
 * @Author xuelongjiang
 */
public class Club implements  Weapon {

    private static Logger logger = LoggerFactory.getLogger(Club.class);

    public void attack() {
        logger.info("木棍伤害为30");
    }
}
