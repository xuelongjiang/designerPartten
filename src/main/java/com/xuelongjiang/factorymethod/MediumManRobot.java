package com.xuelongjiang.factorymethod;

import com.xuelongjiang.simpleFactory.Robot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author xuelongjiang
 */
public class MediumManRobot implements Robot{

    private Logger logger = LoggerFactory.getLogger(MediumManRobot.class);

    public void descrition() {
        logger.info("我是中级的男性机器人");
    }

    public void attack() {
        logger.info("我是中级男性机器人伤害为2");
    }


}
