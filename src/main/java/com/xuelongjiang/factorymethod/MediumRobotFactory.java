package com.xuelongjiang.factorymethod;

import com.xuelongjiang.simpleFactory.Robot;

/**
 * 工厂方法-- 具体的
 * @Author xuelongjiang
 */
public class MediumRobotFactory extends  AbstractMethodFactory {

    @Override
    Robot createRobot(String sex) {

        Robot robot = null;
        if(sex.equals("man")){
            robot = new MediumManRobot();
        }else if(sex.equals("woman")){
            robot = new MediumWomanRobot();
        }
        return robot;

    }
}
