package com.xuelongjiang.factorymethod;

import com.xuelongjiang.simpleFactory.Robot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author xuelongjiang
 */
public class LowerWomanRobot implements Robot {

    private Logger logger = LoggerFactory.getLogger(LowerManRobot.class);

    public void descrition() {
        logger.info("我是低级的女性机器人");
    }

    public void attack() {
        logger.info("我是低级女性机器人伤害为1");
    }
}
