package com.xuelongjiang.factorymethod;

import com.xuelongjiang.simpleFactory.Robot;

/**
 * 工厂方法  抽象的
 * @Author xuelongjiang
 */
abstract class AbstractMethodFactory {

    abstract Robot createRobot(String sex);



}
