package com.xuelongjiang.factorymethod;

import com.xuelongjiang.simpleFactory.Robot;

/**
 * @Author xuelongjiang
 */
public class ExeiciseModel {


    public static void main(String[] args) {

        Robot robot = null;
        LowerRobotFactory lowerRobotFactory = new LowerRobotFactory();
        robot = lowerRobotFactory.createRobot("man");
        robot.descrition();
        robot.attack();

        MediumRobotFactory mediumRobotFactory = new MediumRobotFactory();
        robot = mediumRobotFactory.createRobot("woman");
        robot.descrition();
        robot.attack();


    }

}
