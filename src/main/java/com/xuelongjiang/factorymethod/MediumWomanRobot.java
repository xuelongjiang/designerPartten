package com.xuelongjiang.factorymethod;

import com.sun.org.apache.bcel.internal.generic.LLOAD;
import com.xuelongjiang.simpleFactory.Robot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author xuelongjiang
 */
public class MediumWomanRobot  implements Robot{

    private Logger logger = LoggerFactory.getLogger(MediumWomanRobot.class);

    public void descrition() {
        logger.info("我是中级的女性机器人");
    }

    public void attack() {
        logger.info("我是中级女性机器人伤害为2");
    }
}
