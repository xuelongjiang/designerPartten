package com.xuelongjiang.observer;

import java.util.List;

/**
 *
 * 微信公众号接口
 * @Author xuelongjiang
 */
public interface Wechat {



    public void addSubscribe(User user);

    public void notifySubscribe();

    public void newPaper(String message);

}
