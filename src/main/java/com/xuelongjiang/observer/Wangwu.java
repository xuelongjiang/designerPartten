package com.xuelongjiang.observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 订阅者：王五
 * @Author xuelongjiang
 */
public class Wangwu implements  User {

    Logger logger = LoggerFactory.getLogger(Wangwu.class);

    public Wangwu(Wechat wechat) {
        wechat.addSubscribe(this);
    }

    public void update(String message) {
        logger.info("王五收到订阅消息:  "+message);
    }
}
