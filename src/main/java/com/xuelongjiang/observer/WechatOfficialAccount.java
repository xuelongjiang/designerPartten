package com.xuelongjiang.observer;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 具体的一个公众号
 * 例如：码农翻身
 * @Author xuelongjiang
 */
public class WechatOfficialAccount implements  Wechat {

    List<User> userList;
    private String message;

    public WechatOfficialAccount() {
        this.userList = new ArrayList<User>();
    }


    public void addSubscribe(User user) {
        userList.add(user);

    }

    public void notifySubscribe() {
        for(User user: userList){
            user.update(message);
        }
    }

    public void newPaper(String message) {
        this.message = message;
    }
}
