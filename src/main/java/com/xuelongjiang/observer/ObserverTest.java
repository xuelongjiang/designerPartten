package com.xuelongjiang.observer;

/**
 * @Author xuelongjiang
 */
public class ObserverTest {

    public static void main(String[] args) {

        Wechat wechat = new WechatOfficialAccount();//微信公众号

        Lisi lisi = new Lisi(wechat);//订阅公众号
        Wangwu wangwu  = new Wangwu(wechat);//订阅公众号

        wechat.newPaper("java发布java9版本了");//公众号更新
        wechat.notifySubscribe();//推出给订阅者

    }
}
