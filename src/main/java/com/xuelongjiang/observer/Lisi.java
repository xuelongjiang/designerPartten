package com.xuelongjiang.observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 订阅者：李四
 * @Author xuelongjiang
 */
public class Lisi implements  User {

    Logger logger = LoggerFactory.getLogger(Lisi.class);

    public Lisi(Wechat wechat) {
        wechat.addSubscribe(this);
    }

    public void update(String message) {
            logger.info("李四收到订阅信息: "+message);
    }


}
