package com.xuelongjiang.observer;

/**
 * 订阅者
 *
 * @Author xuelongjiang
 */
public interface User {

    public void update(String message);
}
