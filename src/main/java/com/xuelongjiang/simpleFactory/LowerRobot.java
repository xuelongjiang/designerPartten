package com.xuelongjiang.simpleFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author xuelongjiang
 */
public class LowerRobot implements  Robot {

    private Logger logger = LoggerFactory.getLogger(LowerRobot.class);

    public void descrition() {
        logger.info("我是低等级的机器人");
    }

    public void attack() {
        logger.info("我是低等级机器人伤害为1");
    }
}
