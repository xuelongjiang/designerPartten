package com.xuelongjiang.simpleFactory;

/**
 * @Author xuelongjiang
 */
public class ExerciseModel {


    public static void main(String[] args) {

        User user = new User();
        SimlpeFactory simlpeFactory = new SimlpeFactory();

        Robot robot =  simlpeFactory.createRobot(user.getLevel());
        robot.attack();
    }



}
