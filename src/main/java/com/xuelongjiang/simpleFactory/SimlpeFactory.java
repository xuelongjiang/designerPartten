package com.xuelongjiang.simpleFactory;

/**
 * 简单工厂类
 * @Author xuelongjiang
 */
public class SimlpeFactory {


    //选择创建哪种机器人
    public Robot createRobot(String level){
        Robot robot = null;

        if(level.equals("lower")){
            robot = new LowerRobot();
        }else if(level.equals("medium")){
            robot = new mediumRobot();
        }else if(level.equals("high")){
            robot = new HighRobot();
        }
        robot.descrition();
        return robot;
    }

}
