package com.xuelongjiang.simpleFactory;

/**
 * @Author xuelongjiang
 */
public class User {

   private String level = "lower"; //用户默认为新手

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
