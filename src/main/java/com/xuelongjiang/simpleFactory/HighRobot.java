package com.xuelongjiang.simpleFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author xuelongjiang
 */
public class HighRobot implements  Robot {

    private Logger logger = LoggerFactory.getLogger(HighRobot.class);
    public void descrition() {
        logger.info("我是高等级的机器人");

    }

    public void attack() {
        logger.info("我是高等级机器人伤害为3");
    }
}
