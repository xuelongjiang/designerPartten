package com.xuelongjiang.simpleFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author xuelongjiang
 */
public class mediumRobot implements  Robot {

    private Logger logger = LoggerFactory.getLogger(mediumRobot.class);
    public void descrition() {
        logger.info("我是中等级的机器人");
    }

    public void attack() {
        logger.info("我是中级机器人伤害为2");
    }
}
