package com.xuelongjiang.simpleFactory;

/**
 * 小兵接口
 * 机器人
 * @Author xuelongjiang
 */
public interface Robot {

    public void  descrition();

    public  void attack();
}

